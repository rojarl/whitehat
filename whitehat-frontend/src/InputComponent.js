import React, {Component} from 'react';

const divStyle = {
    display: 'inline-block',
    margin: '0.5em'
}

class MyComponent extends Component {
    constructor(props) {
        super(props);
        console.log(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        };
    }


    handleClick(name)
    {
        alert(name);
    }

    componentWillReceiveProps(props) {
        const url = "http://localhost:80/?brandId="+ props.brandId +"&countryCode="+ props.countryCode;
        fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error)
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded, items} = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Waiting for input...</div>;
        } else {
            return (
                    items.map(item => (
                        <div style={divStyle} className={item.launchcode} onClick={(e) => this.handleClick(item.launchcode)}>
                            <p>{item.name}</p>
                            <img src={item.image_url}/>
                        </div>
                    ))
            );
        }
    }
}

export default MyComponent;