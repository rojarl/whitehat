import React, {Component} from "react";
import logo from "./logo.svg";
import "./App.css";
import MyComponent from "./InputComponent";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {brandId: '', countryCode: ''};

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        this.setState({'brandId': event.target.brandId.value, 'countryCode': event.target.countryCode.value});
        event.preventDefault();
    }

    render() {
        const {brandId, countryCode} = this.state;
        return (
            <div className="App">
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Brand ID:
                        <input name="brandId"/>
                        <br/>
                        Country code:
                        <input name="countryCode"/>
                    </label>
                    <br/>
                    <input type="submit" value="Submit"/>
                </form>
                <MyComponent countryCode={countryCode} brandId={brandId}/>
            </div>
        );
    }
}

export default App;