<?php

namespace AppBundle\Repository;

use AppBundle\Entity\BrandGames;
use AppBundle\Entity\Game;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\ResultSetMapping;

class GameRepository extends EntityRepository
{
    public function findValidGamesByBrandAndCountry($brand, $country, $category = null)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Game::class, 'g');
        $rsm->addFieldResult('g','id', 'id');
        $rsm->addFieldResult('g','name', 'name');
        $rsm->addFieldResult('g','launchcode', 'launchCode');
        $rsm->addFieldResult('g','game_provider_id', 'gameProviderId');
        $rsm->addFieldResult('g','rtp', 'rtp');

        $rsm->addJoinedEntityResult(BrandGames::class, 'bg', 'g', 'brandGames');
        $rsm->addFieldResult('bg', 'bg_id', 'id');
        $rsm->addFieldResult('bg', 'bg_brandid', 'brandId');
        $rsm->addFieldResult('bg', 'hot', 'hot');
        $rsm->addFieldResult('bg', 'new', 'new');
        $rsm->addFieldResult('bg', 'bg_launchcode', 'launchCode');

        $SQL = "
            select game.*,
            bg.id as bg_id, bg.brandid as bg_brandid, bg.launchcode as bg_launchcode, bg.hot, bg.new
            from game
            INNER JOIN brand_games bg on game.`launchcode` = bg.`launchcode`
            LEFT JOIN game_brand_block gbb on gbb.`brandid` = bg.`brandid` and gbb.`launchcode` = game.`launchcode`
            LEFT JOIN game_country_block gcb on gcb.`launchcode`=game.`launchcode` and country = :countryCode and gcb.brandid IN (:brandId,0)
            WHERE bg.brandid = :brandId AND gbb.launchcode is null AND gcb.launchcode is null
        ";

        $params = new ArrayCollection(array(
            new Parameter('brandId', $brand, TYPE::INTEGER),
            new Parameter('countryCode', $country, TYPE::STRING),
        ));

        if(isset($category))
        {
            $SQL .= 'AND bg.category = :category';
            $params->add(new Parameter('category', $category));
        }

        $SQL .= ' GROUP BY game.id, bg.id LIMIT 5';

        $query = $this->_em->createNativeQuery($SQL, $rsm);
        $query->setParameters($params);

        return $query->getResult();
    }
}