<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity()
 * @ORM\Table(name="brand_games")
 */
class BrandGames
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", length=11, unique=true, nullable=false)
     */
    private $id;

    /** @ORM\Column(name="launchcode", type="string", length=50, nullable=false) */
    private $launchCode;

    /** @ORM\Column(name="brandid", type="integer", length=11, nullable=true) */
    private $brandId;

    /** @ORM\Column(name="category", type="string", length=25, nullable=true) */
    private $category;

    /** @ORM\Column(name="hot", type="integer", length=1, nullable=true) */
    private $hot;

    /** @ORM\Column(name="new", type="integer", length=1, nullable=true) */
    private $new;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game", inversedBy="brandGames")
     * @ORM\JoinColumn(name="launchcode", referencedColumnName="launchcode")
     */
    private $game;

    /**
     * @return mixed
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getHot()
    {
        return $this->hot;
    }

    /**
     * @return mixed
     */
    public function getNew()
    {
        return $this->new;
    }
}