<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 * @ORM\Table(name="game")
 */
class Game
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", length=11, unique=true, nullable=false)
     */
    private $id;

    /** @ORM\Column(name="name", type="string", length=100, nullable=true) */
    private $name;

    /** @ORM\Column(name="launchcode", type="string", length=50, nullable=false) */
    private $launchCode;

    /** @ORM\Column(name="game_provider_id", type="integer", length=11, nullable=true) */
    private $gameProviderId;

    /** @ORM\OneToMany(targetEntity="AppBundle\Entity\BrandGames", mappedBy="game") */
    private $brandGames;

    /** @ORM\Column(name="rtp", type="decimal", precision=11, scale=2, nullable=true) */
    private $rtp;

    public function __construct()
    {
        $this->brandGames = new ArrayCollection();
    }
    public function getName()
    {
        return $this->name;
    }

    public function getImage()
    {
        return "https://stage.whgstage.com/scontent/images/games/$this->launchCode.jpg";
    }

    /**
     * @return mixed
     */
    public function getLaunchCode()
    {
        return $this->launchCode;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getBrandGames()
    {
        return $this->brandGames;
    }

    /**
     * @return mixed
     */
    public function getRtp()
    {
        return $this->rtp;
    }

}