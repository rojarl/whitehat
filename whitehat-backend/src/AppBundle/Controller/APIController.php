<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BrandGames;
use AppBundle\Entity\Game;
use AppBundle\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class APIController extends Controller
{
    /**
     * @Route("/")
     * @param GameRepository $gameRepository
     * @return Response
     */
    public function helloWorld(GameRepository $gameRepository, Request $request)
    {
        $brandId = $request->get('brandId');
        $countryCode = $request->get('countryCode');

        if($brandId === null || $countryCode === null)
        {
            throw new BadRequestHttpException();
        }

        $results = $gameRepository->findValidGamesByBrandAndCountry($brandId, $countryCode);

        $return = [];

        /** @var Game $result */
        foreach ($results as $result) {
            $game = [];
            $game['id'] = $result->getId();
            $game['launchcode'] = $result->getLaunchCode();
            $game['name'] = $result->getName();
            $game['image_url'] = $result->getImage();
            $game['rtp'] = $result->getRtp();

            /** @var BrandGames $brandGames */
            $brandGame = $result->getBrandGames()->current();

            $game['hot'] = $brandGame->getHot();
            $game['new'] = $brandGame->getNew();

            $return[] = $game;
        }
        return $this->json($return, 200, ['Access-Control-Allow-Origin' => '*']);
    }
}
