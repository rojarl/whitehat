API Test
========================


What's inside?
--------------

A symfony application with some API endpoints to perform S3 operations.

It has been made taking in account the DDD principles, so there would be no problem if we want to change S3 to any other provider.


Requisites:
-------------
- Docker
- Docker-compose


Installation
--------------
 - Clone the repository in your folder.
 - Execute in the root directory ``composer install -n``
 - Execute in the root directory ``docker-compose build && docker-compose up -d``
 - Now the application will be listening in localhost:80 and you can make requests to it.
 
Endpoints
--------------
This is a test with only 2 endpoints, one to upload podcast episodes to s3, and the other to list the podcasts available.

The natural progression of the api would be accept more than one podcast. By now, it accepts the one with id: 1


#### GET /podcasts/1/episodes

Outputs a list of episodes as JSON. With:
- publish date
- url for download
- episode number
- episode title description
 
#### POST /podcasts/1/episodes
Gets the request and uploads the binary file to S3.

Parameters:
- file: binary file with the mp3.
- episodeTitle: title of the episode
- episodeDescription: description of the episode
- episodeNumber: n� of the episode

